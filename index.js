function initCaching() {
	let counter={}
	return cache = {
		pageAccessCounter:  function (page?){
			if(page){
				if(counter.page.toLowerCase()){
					counter.page.toLowerCase() = counter.page+1;
				} else{
					counter.page.toLowerCase() = 1;
				}
			} else{
				if(counter.home){
					counter.home = counter.home+1;
				} else{
					counter.home = 1;
				}
			}
		}
	
		getCache: function(){
			return counter;
		}
	}
}